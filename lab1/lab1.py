from math import sqrt
from itertools import count, islice
import numpy as np

def is_prime(n):
    if n < 2:
        return False

    for number in islice(count(2), int(sqrt(n) - 1)):
        if n % number == 0:
            return False

    return True


def order_words():
    f = open("Latin-Lipsum.txt", "r")
    line = f.readline()
    words = []
    while line:
        words_from_line = line.split(" ")
        for w in words_from_line:
            if w != "\n":
                words.append(w)
        line = f.readline()
    words.sort()
    print(words)


order_words()