import pickle
import gzip


def read_data():
    data = gzip.open('mnist.pkl.gz', 'rb')
    train_set, valid_set, test_set = pickle.load(data, encoding='latin1')
    data.close()
    return train_set, valid_set, test_set


def parse_data(path):
    f = open(path, "r")
    data = f.read().split("[")
    data = data[1].split("]")
    weights_data = data[0]
    bias = float(data[1])
    while "  " in weights_data:
        weights_data = weights_data.replace("  ", " ").replace("\n", " ")
    weights_data = weights_data.strip(" ").split(" ")
    weights = [float(weights_data[i]) for i in range(0, len(weights_data))]
    return weights, bias

