import numpy as np


def activate(_net_input):
    return (0, 1)[_net_input > 0]


def split_into_mini_batches(training_set, batch_size):
    mini_batches = []
    i = 0
    while len(mini_batches) < len(training_set)/batch_size:
        mini_batch = training_set[i:i+batch_size]
        mini_batches.append(mini_batch)
        i += batch_size
    return mini_batches


# accuracy: 87.42%
def mini_batch_training(training_set, perceptron, batch_size):
    nr_iterations = 1
    mini_batches = split_into_mini_batches(training_set[0], batch_size)
    mini_batches_labels = split_into_mini_batches(training_set[1], batch_size)
    deltai = 0
    betai = 0
    while nr_iterations > 0:
        for i in range(0, len(mini_batches)):
            _input = mini_batches[i]
            _input_labels = mini_batches_labels[i]
            for j in range(0, batch_size):
                t = perceptron.getExpectedOutput(_input_labels[j])
                z = perceptron.netInput(_input[j])
                output = activate(z)
                deltai += (t - output) * _input[j] * 0.1
                betai += (t - output) * 0.1
            perceptron.miniBatchesSetWeights(deltai)
            perceptron.miniBatchesSetBias(betai)
        nr_iterations -= 1


# accuracy: 87.26%
def online_training(training_set, perceptron):
    all_classified = False
    nr_iterations = 1
    _input = training_set[0]
    t = perceptron.getExpectedOutput(training_set[1])

    while all_classified is False and nr_iterations > 0:
        nr_iterations -= 1
        all_classified = True
        z = perceptron.netInput(_input)
        output = activate(z)
        perceptron.adjustWeights(_input, output, t, 0.2)
        perceptron.adjustBias(output, t, 0.2)
        if output != t:
            all_classified = False


class Perceptron:
    def __init__(self, digit, weights, bias):
        self.expected_output = np.zeros(10)
        self.expected_output[digit] = 1
        self.id = digit
        if bias is None:
            self.bias = -1
        else:
            self.bias = bias
        if weights is None:
            self.weights = np.zeros(784)
        else:
            self.weights = np.array(weights)

    def getId(self):
        return self.id

    def getWeights(self):
        return self.weights

    def getBias(self):
        return self.bias

    def getExpectedOutput(self, digit):
        return self.expected_output[digit]

    def adjustBias(self, _output, target, error):
        self.bias += (target - _output) * error

    def adjustWeights(self, _input, _output, target, error):
        self.weights = np.add(self.weights, np.array(_input * (target - _output) * error))

    def netInput(self, _input):
        return np.dot(self.weights, np.array(_input)) + self.bias

    def miniBatchesSetWeights(self, deltai):
        self.weights = np.array(list(map(lambda x: x + deltai, self.weights)))[0]

    def miniBatchesSetBias(self, betai):
        self.bias = betai

    def printToFile(self, path):
        f = open(path, "w")
        f.write(str(self.weights) + "\n" + str(self.bias))
