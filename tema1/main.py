from tema1.FileHelper import read_data, parse_data
from tema1.Perceptron import Perceptron, mini_batch_training, online_training
import os


def train():
    train_set, valid_set, test_set = read_data()
    result_folder = "results/results5"
    # create files for storing perceptrons' weights and bias
    if os.path.exists(result_folder) is False:
        os.mkdir(result_folder)
        for i in range(0, 10):
            f = open(result_folder + "/perceptron_" + str(i) + ".txt", "x")
    # initialize empty perceptrons
    perceptron_list = [Perceptron(i, None, None) for i in range(0, 10)]
    for i in range(0, 10):
        # mini batch training
        mini_batch_training(train_set, perceptron_list[i], 100)
        # online training
        # for j in range(0, len(train_set[0])):
        #     online_training([train_set[0][j], train_set[1][j]], perceptron_list[i])
        perceptron_list[i].printToFile(result_folder + "\perceptron_" + str(i) + ".txt")
    acc = 0
    for i in range(0, len(test_set[1])):
        computed_outputs = [perceptron_list[j].netInput(test_set[0][i]) for j in range(0, 10)]
        index = 0
        for j in range(1, 10):
            if computed_outputs[j] > computed_outputs[index]:
                index = j
        if int(test_set[1][i]) == index:
            acc += 1
    accuracy = acc/len(test_set[1]) * 100
    print(f'{accuracy}% accuracy')


def test():
    result_folder = "results/results5"
    train_set, valid_set, test_set = read_data()
    test_set = valid_set
    trained_perceptrons = []
    # create perceptrons with weights and bias resulted in training
    for i in range(0, 10):
        weights, bias = parse_data(result_folder + "\perceptron_" + str(i) + ".txt")
        trained_perceptrons.append(Perceptron(i, weights, bias))
    acc = 0
    for i in range(0, len(test_set[1])):
        computed_outputs = [trained_perceptrons[j].netInput(test_set[0][i]) for j in range(0, 10)]
        index = 0
        # verificam ce cifra s-a identificat ca fiind
        for j in range(1, 10):
            if computed_outputs[j] > computed_outputs[index]:
                index = j
        # daca crifa identificata = label-ul initial, crestem acuratetea
        if int(test_set[1][i]) == index:
            acc += 1
    accuracy = acc / len(test_set[1]) * 100
    print(f'{accuracy}% accuracy')


# train()
test()
