import gzip
import pickle
import numpy as np
from scipy.stats import bernoulli


def read_data():
    data = gzip.open('mnist.pkl.gz', 'rb')
    train_set, valid_set, test_set = pickle.load(data, encoding='latin1')
    data.close()
    return train_set, valid_set, test_set


def sigmoid(z):
    # functia sigmoid pentru primul si al doilea layer
    return 1/(1 + np.exp(-z))


def softmax(zj, z):
    # functia softmax pentru ultimul layer
    e = np.exp(zj)
    suma = np.sum(np.exp(z))
    return e / suma


# initialise weights according to normal distribution
def init_weights(source_layer_length, target_layer_length, sigma):
    # n - nr de linii (source_layer_length) si m - nr de coloane (target_layer_length)
    # initializare weights cu valori random din distributia normala standard
    return np.random.normal(0, sigma, (source_layer_length, target_layer_length))


def init_biases(layer_length):
    # initializare biases cu valori random din distributia normala standard
    # n - lungimea vectorului = nr de neuroni de pe layeri pt ca fiecare neuron are nevoie de un bias pt. net input(z)
    return np.random.normal(0, 1, layer_length)


def dropout(hidden_layer, threshold):
    data_bern = bernoulli.rvs(size=hidden_layer, p=1-threshold)
    return data_bern


def predict(model, test, no_hidden, no_output):
    W1, b1, W0, b0 = model['W1'], model['b1'], model['W0'], model['b0']

    input_photo = test[0]
    output = test[1]
    n = len(input_photo)
    cnt = 0

    for k in range(n):
        cinput = input_photo[k]
        z1 = cinput.dot(W0) + b0
        y1 = np.zeros(no_hidden)

        for i in range(len(z1)):
            y1[i] = sigmoid(z1[i])

        z2 = y1.dot(W1) + b1
        y3 = np.zeros(no_output)
        for i in range(len(z2)):
            y3[i] = softmax(z2[i], z2)

        if np.argmax(y3) == output[k]:
            cnt += 1

    return cnt / n * 100


def main():
    train_set, valid_set, test_set = read_data()

    np.random.seed(1)
    threshold = 0.2

    # specificatii tema
    n = len(train_set[0])
    iterations = 50
    learning_rate = 0.3
    nr_layers = 3
    input_layer = 784
    hidden_layer = 100
    output_layer = 10
    _lambda = 0.5

    # init weights
    weights = {i: list() for i in range(nr_layers - 1)}
    weights[0] = init_weights(input_layer, hidden_layer, 1 / np.sqrt(input_layer))
    weights[1] = init_weights(hidden_layer, output_layer, 1 / np.sqrt(hidden_layer))

    # init biases
    bias = {i: list() for i in range(nr_layers - 1)}
    bias[0] = init_biases(hidden_layer)
    bias[1] = init_biases(output_layer)

    z2 = np.zeros((n, hidden_layer))  # z values for the second layer
    z3 = np.zeros((n, output_layer))  # z values for the 3rd layer
    y2 = np.zeros((n, hidden_layer))
    y3 = np.zeros((n, output_layer))

    result = dict()
    # For all entries
    for k in range(iterations):
        # choose dropout
        hidden_neurons = dropout(hidden_layer, threshold)
        for i in range(n):
            input_photo = train_set[0][i]
            output = np.zeros(output_layer)
            output[train_set[1][i]] = 1

            # Forward pass - hidden layer
            z2[i] = np.dot(input_photo, weights[0]) + bias[0]
            y2[i] = sigmoid(z2[i])

            # y2[i] *= hidden_neurons
            # y2[i] *= 1/1-threshold

            # Forward pass - last layer
            z3[i] = np.dot(y2[i], weights[1]) + bias[1]
            y3[i] = np.array([softmax(z3[i][j], z3[i]) for j in range(len(z3[i]))])

            # Back propagation
            error_3 = np.array((1 / output_layer) * (y3[i] - output)) # error on last layer
            # [None, :] - creates y axis from vector
            error_2 = y2[i] * (1 - y2[i]) * np.dot(error_3[None, :], weights[1].T)  # error on 2nd layer

            dW1 = y2[i][None, :].T * error_3[None, :]
            dW0 = input_photo[:, None] * error_2

            db1 = -error_3
            db0 = -error_2

            weights[1] -= learning_rate * dW1 - (learning_rate*weights[1]*_lambda)/n
            weights[0] -= learning_rate * dW0 - (learning_rate*weights[0]*_lambda)/n

            bias[1] -= learning_rate * db1
            bias[0] -= learning_rate * np.reshape(db0, hidden_layer)

            result = {'W1': weights[1], 'b1': bias[1], 'W0': weights[0], 'b0': bias[0]}
        print("Accuracy at iteration ", k, " is ", predict(result, test_set, hidden_layer, output_layer))


main()
