import gzip
import pickle
import numpy as np


def read_data():
    data = gzip.open('mnist.pkl.gz', 'rb')
    train_set, valid_set, test_set = pickle.load(data, encoding='latin1')
    data.close()
    return train_set, valid_set, test_set


def sigmoid(z):
    # functia sigmoid pentru primul si al doilea layer
    return 1/(1 + np.exp(-z))


def softmax(zj, z):
    # functia softmax pentru ultimul layer
    e = np.exp(zj)
    suma = np.sum(np.exp(z))
    return e / suma


# initialise weights according to normal distribution
def init_weights(source_layer_length, target_layer_length, sigma):
    # n - nr de linii (source_layer_length) si m - nr de coloane (target_layer_length)
    # initializare weights cu valori random din distributia normala standard
    return np.random.normal(0, sigma, (source_layer_length, target_layer_length))


def init_biases(layer_length):
    # initializare biases cu valori random din distributia normala standard
    # n - lungimea vectorului = nr de neuroni de pe layeri pt ca fiecare neuron are nevoie de un bias pt. net input(z)
    return np.random.normal(0, 0.1, layer_length)


def predict(model, test, no_hidden, no_output):
    W1, b1, W0, b0 = model['W1'], model['b1'], model['W0'], model['b0']

    input = test[0]
    output = test[1]
    n = len(input)
    cnt = 0

    for k in range(n):
        cinput = input[k]
        z1 = cinput.dot(W0) + b0
        y1 = np.zeros(no_hidden)

        for i in range(len(z1)):
            y1[i] = sigmoid(z1[i])

        z2 = y1.dot(W1) + b1
        y3 = np.zeros(no_output)
        for i in range(len(z2)):
            y3[i] = softmax(z2[i], z2)

        if np.argmax(y3) == output[k]:
            cnt += 1

    return cnt / n * 100


def main():
    train_set, valid_set, test_set = read_data()

    # specificatii tema
    n = len(train_set[0])
    iterations = 30
    learning_rate = 0.3
    nr_layers = 3
    input_layer = 784
    hidden_layer = 100
    output_layer = 10

    # L2 regularization - Ridge Regression => reduce overfitting, drive the weights to smaller values

    # init weights
    weights = {i: list() for i in range(nr_layers - 1)}
    weights[0] = init_weights(input_layer, hidden_layer, 1 / np.sqrt(input_layer))
    weights[1] = init_weights(hidden_layer, output_layer, 1 / np.sqrt(hidden_layer))

    # init biases
    bias = {i: list() for i in range(nr_layers - 1)}
    bias[0] = init_biases(hidden_layer)
    bias[1] = init_biases(output_layer)

    z2 = np.zeros((n, hidden_layer))
    z3 = np.zeros((n, output_layer))
    y2 = np.zeros((n, hidden_layer))
    y3 = np.zeros((n, output_layer))

    result = dict()
    # For all entries
    for k in range(iterations):
        for i in range(n):
            input_photo = train_set[0][i]
            output = np.zeros(output_layer)
            output[train_set[1][i]] = 1

            # Forward pass - hidden layer
            z2[i] = np.dot(input_photo.T, weights[0]) + bias[0]
            y2[i] = sigmoid(z2[i])
            # Forward pass - last layer
            z3[i] = np.dot(y2[i], weights[1]) + bias[1]
            y3[i] = np.array([softmax(z3[i][j], z3[i]) for j in range(len(z3[i]))])

            # Back propagation
            error_3 = y3[i] - output  # error on last layer
            # [None, :] - creates y axis from vector
            error_2 = y2[i] * (1 - y2[i]) * np.dot(error_3[None, :], weights[1].T)  # error on 2nd layer

            dW1 = y2[i][:, None] * error_3[None, :]
            dW0 = input_photo[:, None] * error_2
            print(y2[i][:, None].shape)
            print(error_3[None, :].shape)
            # weights[1] *= regularization
            # weights[0] *= regularization

            weights[1] -= learning_rate * dW1
            weights[0] -= learning_rate * dW0

            bias[1] -= learning_rate * error_3
            bias[0] -= learning_rate * np.reshape(error_2, hidden_layer)

            result = {'W1': weights[1], 'b1': bias[1], 'W0': weights[0], 'b0': bias[0]}

        print("Accuracy at iteration ", k / n, " is ", predict(result, test_set, hidden_layer, output_layer))
    print("Accuracy at iteration ", k / n, " is ", predict(result, test_set, hidden_layer, output_layer))


main()
